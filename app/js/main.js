$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > $(window).height() - 400) {
      $("#header").addClass("bg-primary");
      $("#top").css({ display: "flex" });
    } else {
      $("#header").removeClass("bg-primary");
      $("#top").css({ display: "none" });
    }
  });
});

jQuery("#carousel").owlCarousel({
  autoplay: true,
  loop: true,
  margin: 16,
  responsiveClass: true,
  nav: true,
  dots: false,
  responsive: {
    0: {
      items: 1,
    },

    600: {
      items: 2,
    },

    1024: {
      items: 2,
    },

    1366: {
      items: 4,
    },
  },
});

jQuery("#testimonials-slide").owlCarousel({
  // autoplay: true,
  loop: true,
  margin: 0,
  nav: false,
  dots: true,
  items: 1,
});

$("a[href='#top']").click(function () {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});

$("#block-1").click(function () {
  $("html, body").animate(
    {
      scrollTop: $("#block-detail-1").offset().top,
    },
    0
  );
});

$("#block-2").click(function () {
  $("html, body").animate(
    {
      scrollTop: $("#block-detail-2").offset().top,
    },
    0
  );
});

$("#btn-menu-mobile").click(function () {
  $("#menu-mobile-detail").addClass("active");
});

$("#overlay-menu").click(function () {
  $("#menu-mobile-detail").removeClass("active");
});
